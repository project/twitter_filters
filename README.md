CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Twitter Filters module adds two text filters to be used in any text formats
on your Drupal site.

The following text filters are added by this module.
 * Twitter @username converter
 * Twitter #hashtag converter

 * For a full description of the module visit:
   https://www.drupal.org/project/twitter_filters

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/twitter_filters


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Twitter Filters module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Content authoring > Text
       formats and editors to configure the settings.
    3. Select the text format to edit and under "Enabled filters" choose the
       desired filters.
    4. Twitter #hashtag converter, converts Twitter-style #hashtags into links
       to twitter.com.
    5. Twitter @username converter, converts Twitter-style @usernames into links
       to Twitter account pages.
    6. Save configuration.


MAINTAINERS
-----------

 * Swarad Mokal (swarad07) - https://www.drupal.org/u/swarad07

Supporting organization:

 * Axelerant - https://www.drupal.org/axelerant
